var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var minify_css = require('gulp-minify-css');


var config = {
    bowerDir: './bower_components',
    srcDir: './src',
    destDir: './public'
};


gulp.task('html', function () {
    return 1
        && gulp.src(config.destDir + '/*.html')
            .pipe(gulp.dest(config.srcDir));
});

gulp.task('images', function () {
    return 1
        && gulp.src(config.srcDir + '/images/**')
            .pipe(gulp.dest(config.destDir + '/images'));
});

gulp.task('styles', function () {
    return 1
        && gulp.src(config.srcDir + '/less/*.less')
            .pipe(less())
            .pipe(gulp.dest(config.destDir + '/css'))
            .pipe(minify_css())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest(config.destDir + '/css'));
});

gulp.task('scripts', function () {
    return 1
        && gulp
            .src([
                //config.srcDir + '/js/scripts.js',
                //config.srcDir + '/js/outroscript.js',
                config.srcDir + '/js/*.js'
            ])
            .pipe(concat('scripts.js'))
            .pipe(gulp.dest(config.destDir + '/js'))
            .pipe(uglify())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest(config.destDir + '/js'))

        && gulp.src(config.srcDir + '/js/scripts/*.js')
            .pipe(gulp.dest(config.destDir + '/js'))
            .pipe(uglify())
            .pipe(rename({suffix: '.min'}))
            .pipe(gulp.dest(config.destDir + '/js'));
});

gulp.task('vendors', function () {
    return 1
            // off-bower vendors
        && gulp.src(config.srcDir + '/vendors/**')
            .pipe(gulp.dest(config.destDir + '/js/'));
});


gulp.task('components', function () {
    return 1
            // bootstrap
        && gulp.src(config.bowerDir + '/bootstrap/dist/js/**.js')
            .pipe(gulp.dest(config.destDir + '/js'))

        && gulp.src(config.bowerDir + '/bootstrap/fonts/**.*')
            .pipe(gulp.dest(config.destDir + '/fonts'))

            // font-awesome
        && gulp.src(config.bowerDir + '/font-awesome/css/**.*')
            .pipe(gulp.dest(config.destDir + '/css'))

        && gulp.src(config.bowerDir + '/font-awesome/fonts/**.*')
            .pipe(gulp.dest(config.destDir + '/fonts'))

            // jquery
        && gulp.src(config.bowerDir + '/jquery/dist/**.*')
            .pipe(gulp.dest(config.destDir + '/js'))
});

gulp.task('default', ['app']);
gulp.task('all', ['reset']);

gulp.task('app', ['html', 'images', 'styles', 'scripts', 'watch']);
gulp.task('reset', ['components', 'vendors', 'app']);


gulp.task('watch', function () {
    gulp.watch(config.destDir + '/*.html', ['html']);
    gulp.watch(config.srcDir + '/images/**.*', ['images']);
    gulp.watch(config.srcDir + '/less/**.*', ['styles']);
    gulp.watch(config.srcDir + '/js/**.*', ['scripts']);
    gulp.watch(config.srcDir + '/vendors/**.*', ['vendors']);
    gulp.watch(config.bowerDir + '/**.*', ['components']);
});


//var shell = require('gulp-shell');
//gulp.task('less', function () {
//    return gulp.src(paths.less)
//        .pipe(shell([
//            'echo <%= f(file.path) %>',
//            'say "processando <%= f(file.path) %>"'
//        ], {
//            templateData: {
//                f: function (s) {
//                    return s.split('/').reverse()[0];
//                }
//            }
//        }))
//        .pipe(less())
//        .pipe(gulp.dest('./css'));
//});
